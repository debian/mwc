mwc (2.0.4-3) UNRELEASED; urgency=medium

  * d/copyright: Use https protocol in Format field
  * d/control: Set Vcs-* to salsa.debian.org

 -- Ondřej Nový <onovy@debian.org>  Mon, 01 Oct 2018 09:49:33 +0200

mwc (2.0.4-2) unstable; urgency=high

  * debian/mwc.install:
    - Install mwctools.py to /usr/share/mwc (Closes: #877924).
  * New debian/NEWS.Debian about the new config file syntax.
  * New debian/patches/0001-config.patch:
    - Add loading config from every path (Closes: #877927).
  * Change to my new email-address:
    - debian/control,
    - debian/copyright.
  * Remove trailing white spaces:
    - debian/changelog
    - debian/control
  * Declare compliance with Debian Policy 4.1.2.0 (No changes needed).

 -- Jörg Frings-Fürst <debian@jff.email>  Sun, 03 Dec 2017 21:20:52 +0100

mwc (2.0.4-1) unstable; urgency=medium

  * New upstream release:
    - Remove now unusable patches and therefore the printf function
      (Closes: #860494).
    - Add more Parameter needed at some websites (Closes: #862004).
  * Rewrite debian/watch for archives without "v" in front of the version.
  * Declare compliance with Debian Policy 4.1.1. (No changes needed).
  * Bump compatlevel to 10 (no changes required):
    - Change debian/compat to 10.
    - At debian/control change requested version of debhelper to >= 10.
  * At debian/control change Vcs-Browser to secure URI.
  * debian/copyright:
    - Refresh copyright year at * and debian/*.
  * New README.source to explain the branching model used.

 -- Jörg Frings-Fürst <debian@jff-webhosting.net>  Sun, 01 Oct 2017 20:21:11 +0200

mwc (1.7.2-3) unstable; urgency=medium

  * Work for the python3.5 transition (Closes: #799232):
    - debian/control:
      + Simplify Python 3 related build-depends and depends to reflect actual
        requirements.
    - debian/rules:
      + Remove redundant call to dh_python3.
    - Thanks to Scott Kitterman <debian@kitterman.com>.
  * debian/copyright:
    - Add 2015 to debian/*

 -- Jörg Frings-Fürst <debian@jff-webhosting.net>  Sun, 27 Sep 2015 13:12:22 +0200

mwc (1.7.2-2) unstable; urgency=medium

  * debian/control:
    - Replace Depends python-cssselect with python3-cssselect
      (Closes: #764787).
    - Bump Standards-Version to 3.9.6 (no changes required).

 -- Jörg Frings-Fürst <debian@jff-webhosting.net>  Mon, 10 Nov 2014 09:37:29 +0100

mwc (1.7.2-1) unstable; urgency=low

  * Initial release (Closes: #746556)

 -- Jörg Frings-Fürst <debian@jff-webhosting.net>  Thu, 18 Sep 2014 10:49:00 +0200
